from django.db import models


class IPAdress(models.Model):
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    ip = models.GenericIPAddressField(unique=True)

    def __str__(self):
        return self.ip
