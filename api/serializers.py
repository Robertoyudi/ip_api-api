from rest_framework import serializers
from .models import IPAdress


class IPSerializer(serializers.ModelSerializer):
    class Meta:
        model = IPAdress
        fields = ('__all__')
