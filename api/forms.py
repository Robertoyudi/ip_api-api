from django import forms


class IPForm(forms.Form):
    ip = forms.GenericIPAddressField(max_length=45)
