from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse, resolve
from .models import IPAdress
from .views import HomeView
from .forms import IPForm
import requests


class IPModelTest(TestCase):

    def setUp(self):
        ip = '118.69.147.166'
        re = requests.get(
            f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')
        re = re.json()
        IPAdress.objects.create(
            city=re['city'],
            country=re['country'],
            state=re['regionName'],
            ip=re['query'])

    def test_object_atributes_integrity(self):
        location = IPAdress.objects.get(id=1)

        self.assertEqual(location.city, 'Hanoi')
        self.assertEqual(location.country, 'Vietnam')
        self.assertEqual(location.ip, '118.69.147.166')
        self.assertEqual(location.state, 'Hanoi')

    def test_database_count_increase(self):
        self.assertEqual(IPAdress.objects.all().count(), 1)

    def test_database_ip_field_uniqueness(self):
        self.assertRaises(
            IntegrityError,
            lambda: IPAdress.objects.create(
                ip='118.69.147.166'))


class IPFormTest(TestCase):

    def test_form_invalid_ip(self):
        self.form = IPForm(data={'ip': 'a'})
        self.assertEqual(
            self.form.errors['ip'],
            ['Enter a valid IPv4 or IPv6 address.'])

    def test_form_unformatted_ip(self):
        self.form = IPForm(data={'ip': '11869147166'})
        self.assertEqual(
            self.form.errors['ip'],
            ['Enter a valid IPv4 or IPv6 address.'])

    def test_form_null_ip(self):
        self.form = IPForm(data={'ip': ''})
        self.assertEqual(self.form.errors['ip'], ['This field is required.'])

    # def test_form_field_ipv6_normalization(self):
    #     self.form = IPForm(data={'ip':'::ffff:0a0a:0a0a'})
    #     self.assertEqual(self.form.data['ip'],'::ffff:10.10.10.10')


class IPAPITest(TestCase):

    def test_get_ipv4(self):
        ip = '187.84.43.48'
        re = requests.get(
            f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')
        re = re.json()
        self.assertEqual(re['status'], 'success')

    def test_get_ipv6(self):
        ip = '6c49:8cdb:7e3a:775b:4bbc:4a05:9b51:fc78'
        re = requests.get(
            f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')
        re = re.json()
        self.assertEqual(re['status'], 'success')

    def test_get_private_ip(self):
        ip = '192.168.1.1'
        re = requests.get(
            f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')
        re = re.json()
        self.assertEqual(re['message'], 'private range' or 'reserved range')

    # def test_api_request_limit(self):
    #     ip='192.168.1.1'
    #     for x in range(45):
    #         re = requests.get(f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')

    #     self.assertEqual(re.status_code, 429)


class HomeViewTest(TestCase):

    def setUp(self):
        self.response = self.client.get(reverse('homepage'))
        self.view = resolve('/')

    def test_homepage_view(self):
        self.assertEqual(self.response.status_code, 200)
        self.assertTemplateUsed(self.response, 'home.html')
        self.assertContains(self.response, 'IP Locator')
        self.assertNotContains(self.response, 'Horse Porn')
        self.assertEqual(self.view.func.__name__, HomeView.__name__)


class HistoryViewTest(TestCase):

    def setUp(self):
        self.ip_list = [
            '148.14.157.77',
            '187.84.43.48',
            '71.108.119.85',
            '172.34.71.198',
            '162.95.21.244']

        for ip in self.ip_list:
            re = requests.get(
                f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')
            re = re.json()
            IPAdress.objects.create(
                city=re['city'],
                country=re['country'],
                state=re['regionName'],
                ip=re['query'])

        self.response = self.client.get(reverse('history'))

    def test_history_view(self):
        self.assertEqual(self.response.status_code, 200)

    def test_response_is_not_empty(self):
        self.assertTrue(self.response.data)

    def test_all_objects_are_retrieved(self):
        self.assertEquals(len(self.response.data), len(self.ip_list))
