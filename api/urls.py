from django.urls import path
from .views import HomeView, IPAPIView, HistoryView

urlpatterns = [
    path('', HomeView, name='homepage'),
    path('api/v1', IPAPIView.as_view(), name='ipapi'),
    path('history/', HistoryView.as_view(), name='history'),
]
