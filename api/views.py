from django.db import IntegrityError
from django.http import JsonResponse
from rest_framework import generics
from django.shortcuts import redirect, render
from .forms import IPForm
from .models import IPAdress
from .serializers import IPSerializer

import requests


def HomeView(request):
    if request.method == 'POST':
        form = IPForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            ip = cd['ip']
            re = requests.get(
                f'http://ip-api.com/json/{ip}?fields=status,message,country,city,regionName,query')
            re = re.json()
            if re['status'] == 'success':
                try:
                    IPAdress.objects.create(
                        city=re['city'],
                        country=re['country'],
                        state=re['regionName'],
                        ip=re['query'])
                    return redirect('ipapi')
                except IntegrityError:
                    return JsonResponse(
                        {'error': 'This entry is already in the database'})
            else:
                return JsonResponse(re)

    else:
        form = IPForm()
    return render(request, 'home.html', {'form': form})


class IPAPIView(generics.ListAPIView):
    queryset = IPAdress.objects.order_by('-id')[:1]
    serializer_class = IPSerializer


class HistoryView(generics.ListAPIView):
    queryset = IPAdress.objects.all()
    serializer_class = IPSerializer
