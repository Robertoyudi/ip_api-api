## IP_API API
Projeto para o processo seletivo da Mundo_devops.

## Descrição 
Dado um ipv4 ou ipv6 valido, a api retorna a localização do ip informado através dos dados fornecidos pela ip_api, todas consultas unicas são salvas no banco, re-serializadas e expostas pela interface do browsable api do Django Rest framework.

## Instalação:

    #clonar o repositorio
    git clone

    #Instalar as dependecias.
    pip install pipenv 
    pipenv sync
    pipenv shell

    #Setar as variaveis de ambiente
    criar .env file
    preencher .env file:
    export SECRET_KEY=somesecretkey
    export DEBUG=True
    export ALLOWED_HOSTS=localhost,127.0.0.1
    export DATABASE_URL=postgres://postgres:postgres@localhost/postgres


    #Migrar
    python manage.py makemigrations
    python manage.py migrate

    #Rodar o servidor
    python manage.py runserver


## Uso:
digite qualquer ip valido no campo e aperte 'go', veja o historico de resultados clicando em 'history'

## Comentarios:

Dependecias de desenvolvimento e produção não estão 
devidamente separadas no pipfile.

TDD não foi seguido, os testes foram escritos por ultimo.

O passo a passo não foi versionado, o projeto inteiro foi enviado em um unico commit.

## Consultas:
Livro Django for Apis, William S Vincent.

Livro Web Development with Django, Ben Shaw
